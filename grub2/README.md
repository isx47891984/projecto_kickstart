# grub.cfg
Añadimos las entradas propias a grub.cfg  
/boot/grub2/grub.cfg  
```sh
menuentry 'Instalacion FTP' {  
    insmod http  
    insmod net  
        set root='hd0,msdos5'  
        linux  /boot/vmlinuz inst.repo=ftp://192.168.0.10/install/fedora/20/ \  
							 inst.ks=http://192.168.0.10/KickStartNFS.cfg \  
							 inst.vnc inst.vncconnect=192.168.0.10:63596 
        initrd  /boot/initramfs.img  
}

menuentry 'Instalacion HTTP' {  
    insmod http  
    insmod net  
        set root='hd0,msdos5'  
        linux  /boot/vmlinuz inst.repo=http://192.168.0.10/fedora20 \  
							 inst.ks=http://192.168.0.10/KickStartHTTP.cfg \  
							 inst.vnc inst.vncconnect=192.168.0.10:63596  
        initrd  /boot/initramfs.img  
}  

menuentry 'Instalacion NFS' {  
    insmod http  
    insmod net  
        set root='hd0,msdos5'  
        linux  /boot/vmlinuz inst.repo=nfs://192.168.0.10/servidor/nfs/install/fedora/20/ \  
							 inst.ks=http://192.168.0.10/KickStartNFS.cfg \  
							 inst.vnc inst.vncconnect=192.168.0.10:63596  
        initrd  /boot/initramfs.img  
}  
```sh
