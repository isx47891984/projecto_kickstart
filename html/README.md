# HTTP
Instalamos httpd, creamos un link simbolico al directorio de instalacion por http en /var/www/html, copiamos todos los archivos kickstart y levantamos el servicio.  
```sh
# yum install -y httpd  
# ln -s /servidor/http/fedora/20 /var/www/html/fedora20  
# systemctl start httpd
```
