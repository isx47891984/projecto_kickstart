# etc
Contiene:
etc
├── dhcp
│   ├── dhcpd.conf
├── exports
├── network-scripts
│   └── ifcfg-p5p1
├── README.md
├── vsftpd
│   ├── README.md
│   └── vsftpd.conf
└── xinetd.d
    └── tftp

