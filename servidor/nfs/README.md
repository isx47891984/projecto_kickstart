# NFS
Instalamos nfs-utils.  
Añadimos el directorio /servidor/nfs al archivo /etc/exports y lo exportamos.  
```sh  
# yum install -y nfs-utils  
# echo ‘/servidor/nfs 192.168.0.0/16(ro,subtree_check)’ >> /etc/exports  
# exportfs -a  
# exportfs  
    /servidor/nfs     192.168.0.0/16  
```
