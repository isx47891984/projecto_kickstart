#!/bin/sh
#NetworkManager s'engega automaticament amb l'ordinador
systemctl enable NetworkManager-wait-online.service
#Parem el servei firewalld i el desactivem
systemctl stop firewalld
systemctl disable firewalld
#Instal·lem el components necessaris per el dia a dia a classe
cd /tmp
wget ftp://192.168.0.10/paquetes/instala_paquets.sh
chmod +x instala_paquets.sh
./instala_paquets.sh
#Desactivem selinux
sed 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
#Excloem actualitzacions de kernel
echo "exclude=kernel*" >> /etc/yum.conf
#Configurem kerberos y LDAP, --update actualitza nomes arxius als que els afecten els cambis
authconfig --update \
		   --enableldap --ldapserver="ldap" --ldapbasedn="dc=escoladeltreball,dc=org" \
		   --enablekrb5 --krb5realm="INFORMATICA.ESCOLADELTREALL.ORG" --krb5kdc="gandhi"
wget ftp://192.168.0.10/paquetes/kerberos-nfs-f20.tgz
tar -C / -xvzf kerberos-nfs-f20.tgz
dconf update
systemctl enable nfs-secure.service
systemctl enable gpm
#Instal·lar repositorios
rpm -ivh http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-stable.noarch.rpm
rpm -ivh http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-stable.noarch.rpm
rpm -ivh http://linuxdownload.adobe.com/adobe-release/adobe-release-x86_64-1.0-1.noarch.rpm
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-adobe-linux
yum -y install flash-plugin
#Configurar Grub
sed -i 's/set default="${saved_entry}"/set default=0/' /boot/grub2/grub.cfg
sed -i 's/set timeout=5/set timeout=-1/' /boot/grub2/grub.cfg
sed -i "s/'Fedora, with Linux 3.11.10-301.fc20.x86_64'/'MATI'/" /boot/grub2/grub.cfg
sed -i "s/'Fedora, with Linux 0-rescue.*'/'MATI-rescue'/" /boot/grub2/grub.cfg
#Editar fstab
echo "gandhi:/groups/ /home/groups  nfs4  sec=krb5,comment=systemd.automount  0  0 " >> /etc/fstab
##
