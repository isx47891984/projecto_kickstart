#!/bin/sh

yum -y install  krb5-devel
yum -y install krb5-workstation
yum -y install nfs-utils
yum -y install pam_mount
yum -y install gpm
yum -y install terminator
yum -y install geany
yum -y install vim
yum -y install gvim
yum -y install vim-minimal
